Form @document_extension
========================

	'use strict'


	{utils, assert, signal, Document, Networking} = Neft
	{Element} = Document
	{Tag} = Element

	module.exports = (config) ->
		Document.Element.Tag.extensions.form = class Form extends Tag
			@__name__ = 'Form'
			@__path__ = 'File.Element.Tag.extensions.form'

			JSON_CTOR_ID = @JSON_CTOR_ID = Element.JSON_CTORS.push(Form) - 1
			JSON_ARGS_LENGTH = @JSON_ARGS_LENGTH = Tag.JSON_ARGS_LENGTH

			INPUT_TYPES_TO_ARRAY = ['radio']
			ARRAY_VALUES_NAME_RE = /\[\]$/

			@_fromJSON = (arr, obj=new Form) ->
				Tag._fromJSON arr, obj

*Form* Form() : *Document.Element.Tag*
--------------------------------------

			addElement = (node) ->
				if name = node.attrs.name
					if node.attrs.type in INPUT_TYPES_TO_ARRAY
						unless isFormInputArray(@elements[name])
							@elements[name] = createFormInputArray()
						@elements[name].push node
						if node.attrs.checked
							@elements[name].checkedElement = node
					else if ARRAY_VALUES_NAME_RE.test(name)
						name = name.slice 0, -2
						@elements[name] ?= []
						@elements[name].push node
					else
						@elements[name] = node

					if selected = node.query('option[selected=true]')
						node.attrs.set 'value', selected.attrs.value
				return

			removeElement = (node, name) ->
				name ?= node.attrs.name

				if name
					element = @elements[name]
					if node.attrs.type in INPUT_TYPES_TO_ARRAY
						utils.remove element, node
						if element.checkedElement is node
							element.checkedElement = null
					else if ARRAY_VALUES_NAME_RE.test(name)
						name = name.slice 0, -2
						element = @elements[name]
						utils.remove element, node
					if element is node or (Array.isArray(element) and element.length <= 0)
						delete @elements[name]
				return

			createOnElementAttrsChange = (form) ->
				(name, oldValue) ->
					if name is 'name'
						removeElement.call form, this, oldValue
						addElement.call form, this
					return

			# Context: Form
			onAddInputNode = (node) ->
				node.onAttrsChange @_onElementAttrsChange
				addElement.call @, node
				return

			# Context: Form
			onRemoveInputNode = (node) ->
				node.onAttrsChange.disconnect @_onElementAttrsChange
				removeElement.call @, node
				return

			# Context: Form
			onSelectInputNode = (node) ->
				arr = @elements[node.attrs.name]
				if isFormInputArray(arr)
					if arr.checkedElement is node
						return
					arr.checkedElement?.attrs.set 'checked', false
					arr.checkedElement = node
				return

			# Context: Form
			onUnselectInputNode = (node) ->
				arr = @elements[node.attrs.name]
				if isFormInputArray(arr) and arr.checkedElement is node
					arr.checkedElement = null
				return

			# Context: Form
			onSelectOptionNode = (node) ->
				select = node.queryParents 'select'
				if node.attrs.has('value')
					value = node.attrs.value
				else
					value = node.stringifyChildren()
					node.attrs.set 'value', value
				options = select.queryAll 'option[selected=true]'
				for option in options
					if option isnt node
						option.attrs.set 'selected', false
				select.attrs.set 'value', value
				return

			# Context: Form
			onUnselectOptionNode = (node) ->
				unless select = node.queryParents 'select'
					return
				value = node.attrs.value
				if select.attrs.value is value
					select.attrs.set 'value', ''
				return

			constructor: ->
				super()
				@elements = {}
				@_onElementAttrsChange = createOnElementAttrsChange this

				# input
				watcher = @watch 'input, textarea, select'
				watcher.onAdd onAddInputNode, @
				watcher.onRemove onRemoveInputNode, @

				watcher = @watch "input[checked=true]"
				watcher.onAdd onSelectInputNode, @
				watcher.onRemove onUnselectInputNode, @

				watcher = @watch 'option[selected=true]'
				watcher.onAdd onSelectOptionNode, @
				watcher.onRemove onUnselectOptionNode, @

				`//<development>`
				if @constructor is Form
					Object.seal @
				`//</development>`

*Object* Form::elements
-----------------------

*Signal* Form::onLoadEnd(*Any* error, *Any* data)
-------------------------------------------------

			signal.Emitter.createSignal @, 'onLoadEnd'

*Signal* Form::onBeforeSubmit(*Networking.Request* req)
-------------------------------------------------------

			signal.Emitter.createSignal @, 'onBeforeSubmit'

*Object* Form::getData([*Object* target])
-----------------------------------------

			getData: do ->
				addNodeValue = (node, target, targetProp) ->
					unless node.attrs.disabled
						value = node.attrs.value
						if node.attrs.has('checked')
							if node.attrs.checked
								if not value?
									value = true
							else
								if Array.isArray(target)
									return
								if not value? or typeof value is 'boolean'
									value = false
								else
									value = ''
						target[targetProp] = value
					return
				(data={}) ->
					for name, node of @elements
						if isFormInputArray(node)
							unless node = node.checkedElement
								continue
							addNodeValue node, data, name
						else if Array.isArray(node)
							arr = data[name] = []
							for subnode in node
								addNodeValue subnode, arr, arr.length
						else
							addNodeValue node, data, name
					data

*Networking.Request* Form::submit()
-----------------------------------

			submit: ->
				req = new Networking.Request
					method: @attrs.method or 'get'
					uri: @attrs.action or ''
					data: @getData()
					onLoadEnd: (err, data) =>
						@onLoadEnd.emit err, data
				if @onBeforeSubmit.emit(req) isnt signal.STOP_PROPAGATION
					config.app.networking.createLocalRequest req

			clone: (clone = new Form) ->
				super clone

			toJSON: (arr) ->
				unless arr
					arr = new Array JSON_ARGS_LENGTH
					arr[0] = JSON_CTOR_ID
				super arr
				arr

*FormInputArray* FormInputArray : *Array*
-----------------------------------------

		class FormInputArray
			constructor: ->
				return createFormInputArray()

		createFormInputArray = ->
			arr = []
			arr.constructor = FormInputArray
			arr.checkedElement = null
			arr

		isFormInputArray = (arg) ->
			arg and arg.constructor is FormInputArray

ReadOnly *Document.Element.Tag* FormInputArray::checkedElement
--------------------------------------------------------------
